sentence = input('Enter your sentence: ')
lowercase = sentence.lower()

vowel_counts = {}

for vowel in "aeiou":
    count = lowercase.count(vowel)
    vowel_counts[vowel] = count

counts = vowel_counts.values()

total_vowels = sum(counts)

the_values = vowel_counts.values()


def del_n(n, s):
    so_far = 1
    previous = s[0]
    res = [s[0]]

    for idx, c in enumerate(s[1:]):
        if c == previous:
            so_far += 1
            if so_far >= n + 1:
                continue
        else:
            previous = c
            so_far = 1
        res.append(c)
    return ' dan '.join(res)


def listToString(s):
    str1 = ""

    for i in lowercase:
        if (i == 'a' or i == 'e' or i == 'i' or i == 'o' or i == 'u'):
            str1 += i
    word = del_n(1, str1)
    return word


print(sentence + " = ", total_vowels, " yaitu ", listToString(sentence))


